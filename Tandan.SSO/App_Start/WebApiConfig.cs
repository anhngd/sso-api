﻿using System.Web.Http;

namespace Tandan.SSO
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "GetTokenAPI",
                routeTemplate: "api/{controller}/{action}/{usr}",
                defaults: new { controller = "Tokens", action = "" }
            );

            config.Routes.MapHttpRoute(
               name: "SetTokenAPI",
                routeTemplate: "api/{controller}/{action}/{usr}/{pwd}",
                defaults: new { controller = "Tokens", action = "Get" }
            );
            config.Routes.MapHttpRoute(
                name: "Get2TokenAPI",
                routeTemplate: "api/{controller}/{action}/{key}/{usr}",
                defaults: new { controller = "Tokens", action = "" }
            );
            config.Routes.MapHttpRoute(
                name: "ChangePasswordAPI",
                routeTemplate: "api/{controller}/{action}/{userName}/{oldPassword}/{newPassword}",
                defaults: new { controller = "Tokens", action = "" }
            );
        }
    }
}
