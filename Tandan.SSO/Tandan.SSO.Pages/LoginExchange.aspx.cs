﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Tandan.SSO.Pages
{
    public partial class LoginExchange : System.Web.UI.Page
    {
        private const string Cashost = "https://sso.hatinh.dcs.vn/cas/";
        private const string Casapi = "https://sso1.hatinh.dcs.vn/";
        const string PrvKey = @"<RSAKeyValue><Modulus>uzXJQfjzcl4NvWz3t5iW908EMnjDvXmdKk3lt+9+3U9h8+ODx8yCdE/JJhlXp9OD++jALGM165xAcR4JTu3HSenvVhdoceFRMHuc4kfYwlyL2uU6Br97huC8XVT6R3eRhx6Ya4lq/7h+5fd73EaGG9p5yzl9wukFVCY/XMXkTKG4eWXcZs1Qhl9O+shtVSCL2WjZviAD1HPnYnt9F7NxdPb5OH0g7JbamKvg4HBPeaVJitIC7kGDaU7ifd1IgJ7Zxa6SLXs6Q9rERxYtBxCpKSt5PaOH0iIcGRrp3hHqGBewWsW8J4M7eb5BiN2RVYIpe5T/patnKfGgeADBcSRAjQ==</Modulus><Exponent>AQAB</Exponent><P>3IS2opQ1503LfmL9VRkWD218gCFbS/dAWBvewFQNim1gsDfyBYiHpWZ/Qa6Ea1Ky26gkodT71kujx5H97bMfJdBxwSOGodsnJIzXM2M5SAjZDNZSumUFQKUJYS0tOaqwgJ9isFiAhxLPc84s9TJsUA57UqCUZfEganlQXlhDmh0=</P><Q>2VUVVqSVkGYnpVKqIkyazDgDDoGuSJN1i+qEsoWrdEmVMCOAYBj9VqltK29wK48DrhpI5VmnAxlUhHCkUXqxdoW95TLJLILXiUVj4fNZGoXpqHtcqG+Nv6PcClUAJydI8qWent4llSm/y8JVBUvlie4YPdu7Hku0eeUH6GKu9TE=</Q><DP>Cgz2cgSFTGyMSM9kMnu5OKzJzuE2UwkK4rkqduOd1mBT1tRF8KCfZhd811aH7IhRZJeOS930OHksFs19oFbYpZWRibb/zOK7LvxIVqxAERtuIRqnnJ08Fw0ul0JC3Gz+LG6XRehSu1VNJT8RZ3+L1j5Cuqpc+SR7UL8EvbgSJLU=</DP><DQ>F7KJUxs7G7Wn2QT8fCTyNBZfbvOA93TsukbmFTkyCsoDNG6Kju/qn8p1lujdyEId2pwPGsL9zP60EoTdxeyk8RGANT+mY+4YZmVkkLyiGJxFcBOTdOJFCIJreczu6VYFRD/+3b/HRU56FVOpRZyS+Mk3Z3dQHeuPUENC63nnLeE=</DQ><InverseQ>y3mM+MBKSmQIB+jqVB3QdpozaadvOsQTzcrPNZAdaxceiYY1b/3H+OGxXbY9jCXqwmGMteEl9cnRsUcOyipRk1ffr7SSV2nxyr5D9+e8bYlE2HYHGDiZIuG2GzgsVvcFIdg+rV79lRXIBlnileUJlZtR5tO9JWHjFBsWx6DXG8o=</InverseQ><D>CjbeTwDuCXqrvb7dCjGjZQV06WsQ5PzmcBk1dDJ3Rcxvv/6VIVjdckqrIy7FS1UdUaiKmbFlDwCkvHdC00FievwuUJBw5KvCQgawkeERKNOjVtNkddtLOln3NRFuoWka8dgfwVhLU/4FQHdfFO7tIMZsGERAAhs0LPn0QZPTh8HAOuSK0mcL5Tk9KCWq/YAmyUJMlIMGGewp40O1hVmtmoPVTzQncWpdbbEieAVHllSStrSS0urthFSUG8+mC9RQYk22wPPvzGgUepY8pebR49lc/ToCLap5/kN47DsdKUv7kb04o3CxJGW08g/PPFj4sWmK3uMST9UeRzkbzuaJsQ==</D></RSAKeyValue>";
        
        public static CspParameters GetCspParameters()
        {
            var lCspParams = new CspParameters();
            lCspParams.Flags |= CspProviderFlags.UseMachineKeyStore;   // all users of this computer have access
                                                                       //lCspParams.Flags |= CspProviderFlags.UseUserProtectedKey;  // a popup window will ask for confirmation
                                                                       //lCspParams.Flags |= CspProviderFlags.UseNonExportableKey;  // you can use the key, but not read or export it
            lCspParams.KeyContainerName = "MySecretKeyContainer";
            return lCspParams;
        } //

        /// <summary>
        /// Decryption pwd
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string Decryption(string strText)
        {
            var rsaParams = new CspParameters { Flags = CspProviderFlags.UseMachineKeyStore };
            using (var rsa = new RSACryptoServiceProvider(2048, rsaParams))
            {
                try
                {
                    var base64Encrypted = strText.Replace(@"\/", "/").Replace("\"", "");
                    RSACryptoServiceProvider.UseMachineKeyStore = false;
                    // server decrypting data with private key                    
                    rsa.FromXmlString(PrvKey);
                    var resultBytes = Convert.FromBase64String(base64Encrypted);
                    var decryptedBytes = rsa.Decrypt(resultBytes, true);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //using (System.IO.StreamWriter file =
            //new System.IO.StreamWriter(@"C:\\log" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt"))
            //{
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                var mac = GetMacAddress();

                // Look for the "ticket=" after the "?" in the URL

                var tkt = Request.QueryString["ticket"];
                // This page is the CAS service=, but discard any query string residue
                //string service = Request.Url.GetLeftPart(UriPartial.Authority);
                //var service = Request.Url.GetLeftPart(UriPartial.Path);
                //var service = Request.Url.AbsoluteUri;
                // First time through there is no ticket=, so redirect to CAS login
                var service = Request.Url.AbsoluteUri;
                var query = Request.Url.Query;

                try
                {
                    service = service.Replace(query, string.Empty);
                }
                catch { }
                if (string.IsNullOrEmpty(tkt))
                {
                    var redir = Cashost + "login?" +
                      "service=" + service + "&m=" + mac + "&r=" + query;
                    Response.Redirect(redir);
                }
                else
                {
                    var validateurl = Cashost + "p3/serviceValidate?" +
                      "ticket=" + tkt + "&" +
                      "service=" + service;
                    var client = new WebClient();
                    var str = client.OpenRead(validateurl);
                    if (str == null) return;
                    var reader = new StreamReader(str);
                    var resp = reader.ReadToEnd();
                    var xDoc = new XmlDocument();
                    xDoc.LoadXml(resp);
                    var usrName = string.Empty;
                    try { usrName = xDoc.GetElementsByTagName("cas:user")[0].InnerText; }
                    catch
                    {
                        // ignored
                    }
                    if (string.IsNullOrEmpty(usrName)) Response.Redirect("/");
                    var webClient = new WebClient();
                    var apiPath = Casapi + "api/tokens/get/" + usrName;
                    var apiStr = webClient.OpenRead(apiPath);
                    if (apiStr == null) return;
                    var apiReader = new StreamReader(apiStr);
                    var dePwd = apiReader.ReadToEnd();
                    var pwd = Decryption(dePwd);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CallBackFunc", string.Format("LoginToOWA('{0}', '{1}', '{2}', '{3}');", "mail.ninhthuan.gov.vn", "", usrName, pwd), true);
                    
            }
            
        }

        

        private string GetMacAddress()
        {
            var macAddresses = string.Empty;

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up))
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }

            return macAddresses;
        }
    }
}