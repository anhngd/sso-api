﻿using System;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Configuration;
using System.DirectoryServices;

namespace Tandan.SSO.Controllers
{
    public class TokensController : ApiController
    {
        readonly string _connStr = ConfigurationManager.AppSettings["DBConnection"];
        const string PrvKey = "<RSAKeyValue><Modulus>uzXJQfjzcl4NvWz3t5iW908EMnjDvXmdKk3lt+9+3U9h8+ODx8yCdE/JJhlXp9OD++jALGM165xAcR4JTu3HSenvVhdoceFRMHuc4kfYwlyL2uU6Br97huC8XVT6R3eRhx6Ya4lq/7h+5fd73EaGG9p5yzl9wukFVCY/XMXkTKG4eWXcZs1Qhl9O+shtVSCL2WjZviAD1HPnYnt9F7NxdPb5OH0g7JbamKvg4HBPeaVJitIC7kGDaU7ifd1IgJ7Zxa6SLXs6Q9rERxYtBxCpKSt5PaOH0iIcGRrp3hHqGBewWsW8J4M7eb5BiN2RVYIpe5T/patnKfGgeADBcSRAjQ==</Modulus><Exponent>AQAB</Exponent><P>3IS2opQ1503LfmL9VRkWD218gCFbS/dAWBvewFQNim1gsDfyBYiHpWZ/Qa6Ea1Ky26gkodT71kujx5H97bMfJdBxwSOGodsnJIzXM2M5SAjZDNZSumUFQKUJYS0tOaqwgJ9isFiAhxLPc84s9TJsUA57UqCUZfEganlQXlhDmh0=</P><Q>2VUVVqSVkGYnpVKqIkyazDgDDoGuSJN1i+qEsoWrdEmVMCOAYBj9VqltK29wK48DrhpI5VmnAxlUhHCkUXqxdoW95TLJLILXiUVj4fNZGoXpqHtcqG+Nv6PcClUAJydI8qWent4llSm/y8JVBUvlie4YPdu7Hku0eeUH6GKu9TE=</Q><DP>Cgz2cgSFTGyMSM9kMnu5OKzJzuE2UwkK4rkqduOd1mBT1tRF8KCfZhd811aH7IhRZJeOS930OHksFs19oFbYpZWRibb/zOK7LvxIVqxAERtuIRqnnJ08Fw0ul0JC3Gz+LG6XRehSu1VNJT8RZ3+L1j5Cuqpc+SR7UL8EvbgSJLU=</DP><DQ>F7KJUxs7G7Wn2QT8fCTyNBZfbvOA93TsukbmFTkyCsoDNG6Kju/qn8p1lujdyEId2pwPGsL9zP60EoTdxeyk8RGANT+mY+4YZmVkkLyiGJxFcBOTdOJFCIJreczu6VYFRD/+3b/HRU56FVOpRZyS+Mk3Z3dQHeuPUENC63nnLeE=</DQ><InverseQ>y3mM+MBKSmQIB+jqVB3QdpozaadvOsQTzcrPNZAdaxceiYY1b/3H+OGxXbY9jCXqwmGMteEl9cnRsUcOyipRk1ffr7SSV2nxyr5D9+e8bYlE2HYHGDiZIuG2GzgsVvcFIdg+rV79lRXIBlnileUJlZtR5tO9JWHjFBsWx6DXG8o=</InverseQ><D>CjbeTwDuCXqrvb7dCjGjZQV06WsQ5PzmcBk1dDJ3Rcxvv/6VIVjdckqrIy7FS1UdUaiKmbFlDwCkvHdC00FievwuUJBw5KvCQgawkeERKNOjVtNkddtLOln3NRFuoWka8dgfwVhLU/4FQHdfFO7tIMZsGERAAhs0LPn0QZPTh8HAOuSK0mcL5Tk9KCWq/YAmyUJMlIMGGewp40O1hVmtmoPVTzQncWpdbbEieAVHllSStrSS0urthFSUG8+mC9RQYk22wPPvzGgUepY8pebR49lc/ToCLap5/kN47DsdKUv7kb04o3CxJGW08g/PPFj4sWmK3uMST9UeRzkbzuaJsQ==</D></RSAKeyValue>";
        
        // GET api/tokens/5
        [HttpGet]
        [ActionName("Get")]
        public string GetToken(string usr)
        {
            var pwd = string.Empty;
            //return _connStr + "---";
            
            var usr1 = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(usr));
            using (var conn = new SqlConnection(_connStr))
            {
                conn.Open();
                var insertStr = "SELECT * FROM LoginHistory WHERE UserName = '" + usr1 + "' ORDER BY CreatedTime DESC";
                using (var command = new SqlCommand(insertStr, conn))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        pwd = reader["Password"].ToString();
                        if (!string.IsNullOrEmpty(pwd))
                            break;
                    }
                }
            }
            return pwd;
        }

        [HttpPost]
        [ActionName("Get2")]
        public string GetToken2(string usr, string key)
        {
            if (key != "EAAE30DE-06C5-43B7-BDCC-CDA3FA1B2E4C") return string.Empty;
            var pwd = GetToken(usr);
            pwd = Decryption(pwd);
            return pwd;
        }

        [HttpPost]
        [ActionName("Set")]
        public bool SetToken(string usr, string pwd)
        {
            var usr1 = Encoding.UTF8.GetString(System.Convert.FromBase64String(usr));
            var pwd1 = Encoding.UTF8.GetString(System.Convert.FromBase64String(pwd));
            var encrypted = Encryption(pwd);
            try {
                using (var conn = new SqlConnection(_connStr))
                {
                    conn.Open();
                    var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    var insertStr = "INSERT INTO LoginHistory(UserName, Password, CreatedTime) VALUES('" + usr1 + "', '" + encrypted + "', '" + dt + "');";
                    using (var command = new SqlCommand(insertStr, conn))
                    {
                        command.ExecuteReader();
                    }
                }
                return true;
            }
            catch (Exception ex) { return false; }
        }

        [HttpPost]
        [ActionName("ChangePassword")]
        public string ChangePassword(string userName, string oldPassword, string newPassword)
        {
            string adminUser = ConfigurationManager.AppSettings["ADAdminUser"];
            string adminPassword = ConfigurationManager.AppSettings["ADAdminPassword"];
            string fullPath = ConfigurationManager.AppSettings["ADServer"];

            var usr = Encoding.UTF8.GetString(System.Convert.FromBase64String(userName));
            var oldPwd = Encoding.UTF8.GetString(System.Convert.FromBase64String(oldPassword));
            var newPwd = Encoding.UTF8.GetString(System.Convert.FromBase64String(newPassword));
            try
            {
                try
                {
                    DirectoryEntry entry = new DirectoryEntry(fullPath, usr, oldPwd, AuthenticationTypes.Secure);
                    DirectorySearcher ds = new DirectorySearcher(entry);
                    var a = ds.FindOne();
                }
                catch { return "Mật khẩu cũ không đúng, vui lòng nhập lại"; }

                // Change Password
                DirectoryEntry domainEntry = new DirectoryEntry(fullPath, adminUser, adminPassword, AuthenticationTypes.Secure);
                DirectorySearcher dirSearcher = new DirectorySearcher(domainEntry);
                string filter = string.Format("(SAMAccountName={0})", usr);
                dirSearcher.Filter = filter;
                SearchResult result = dirSearcher.FindOne();
                if (result != null)
                {
                    DirectoryEntry userEntry = result.GetDirectoryEntry();

                    // Enable Account if it is disabled
                    userEntry.Properties["userAccountControl"].Value = 0x200;
                    // Reset User Password
                    userEntry.Invoke("SetPassword", new object[] { newPwd });
                    // Force user to change password at next logon
                    // userEntry.Properties["pwdlastset"][0] = 0;                    
                    userEntry.CommitChanges();
                    userEntry.RefreshCache();
                    userEntry.Close();
                }
                else
                {
                    // User not found
                }
                return "Thay đổi mật khẩu thành công. Hệ thống sẽ tự động quay về trang chủ";
            }
            catch (Exception ex) { return "Đã có lỗi xảy ra, vui lòng liên hệ quản trị viên"; }
        }


        private static string Encryption(string strText)
        {
            const string publicKey = "<RSAKeyValue><Modulus>uzXJQfjzcl4NvWz3t5iW908EMnjDvXmdKk3lt+9+3U9h8+ODx8yCdE/JJhlXp9OD++jALGM165xAcR4JTu3HSenvVhdoceFRMHuc4kfYwlyL2uU6Br97huC8XVT6R3eRhx6Ya4lq/7h+5fd73EaGG9p5yzl9wukFVCY/XMXkTKG4eWXcZs1Qhl9O+shtVSCL2WjZviAD1HPnYnt9F7NxdPb5OH0g7JbamKvg4HBPeaVJitIC7kGDaU7ifd1IgJ7Zxa6SLXs6Q9rERxYtBxCpKSt5PaOH0iIcGRrp3hHqGBewWsW8J4M7eb5BiN2RVYIpe5T/patnKfGgeADBcSRAjQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

            var testData = Encoding.UTF8.GetBytes(strText);

            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                try
                {
                    // client encrypting data with public key issued by server                    
                    rsa.FromXmlString(publicKey);

                    var encryptedData = rsa.Encrypt(testData, true);

                    var base64Encrypted = Convert.ToBase64String(encryptedData);

                    return base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        private static string Decryption(string strText)
        {
            var rsaParams = new CspParameters { Flags = CspProviderFlags.UseMachineKeyStore };
            using (var rsa = new RSACryptoServiceProvider(2048, rsaParams))
            {
                try
                {
                    var base64Encrypted = strText.Replace(@"\/", "/").Replace("\"", "");
                    RSACryptoServiceProvider.UseMachineKeyStore = false;
                    // server decrypting data with private key                    
                    rsa.FromXmlString(PrvKey);
                    var resultBytes = Convert.FromBase64String(base64Encrypted);
                    var decryptedBytes = rsa.Decrypt(resultBytes, true);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
                
    }
}
